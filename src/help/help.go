package help

import (
	"fmt"
	"sbackup/src/commandLine"
)

var helpGeneral = `
sbackup is a simple backup tool that uses rclone for copying files to several
destinations. You can define backup configurations with different source paths
and different destination paths.

If you want to use a remote location as destination, you have to define this
location in the rclone configuration first.

sbackup does not execute a backup job itself. You have to execute the backup
command either manually or with a tool like cron.

Usage:
  sbackup [command]

Available Commands:
  backup    Run a specific backup
  clean     Clean up the backup destination
  help      Show help for sbackup
  validate  Validate configuration file
`

var helpValidate = `
This command validates you configuration file. It checks if the file
has any syntax errors or if any keys are missing or misplaced.

Can can optionally specify an absolut or relative path to your configuration
file. If you do not specify a path, sbackup uses /etc/sbackup.conf as default
path.

Usage:
  sbackup validate [flags]

Flags:
  -c [path], --configuration=[path] Path to your configuration file. If omitted,
                                    defaults to /etc/sbackup.conf
  -h,        --help                 Show help for this command
`

var helpBackup = `
This command starts a backup job. You specify the name of the backup job after
the backup parameter. The name has to be defined in the given configuration file.

Usage:
  sbackup backup name [flags]

Flags:
  -c [path], --configuration=[path] Path to your configuration file. If omitted,
                                    defaults to /etc/sbackup.conf
  -h,        --help                 Show help for this command
`

var helpClean = `
This command cleans the backup destination. If a backup folder contains more backups,
than specified in the configuration file, the oldest backups will be deleted.

Usage:
  sbackup clean [flags]

Flags:
  -c [path], --configuration=[path] Path to your configuration file. If omitted,
                                    defaults to /etc/sbackup.conf
  -h,        --help                 Show help for this command
`

var helpFooter = `
Use "sbackup help" for an overview about commands.
Use "sbackup [command] --help" for more information about a command.`

func PrintHelp(command commandLine.Command) {
	switch command {
	case commandLine.Validate:
		fmt.Print(helpValidate)
	case commandLine.Backup:
		fmt.Print(helpBackup)
	case commandLine.Clean:
		fmt.Print(helpClean)
	default:
		fmt.Print(helpGeneral)
	}
	fmt.Print(helpFooter)
}
