package commandLine

import (
	"errors"
	"fmt"
)

func parseBackupCommandLine(cmd *CommandLine, args map[string]string) error {
	if helpParameterAnywhere(args) {
		cmd.ShowHelp = true
		return nil
	}

	backupName := ""

	for key, value := range args {
		switch key {
		case "-c", "--configuration":
			cmd.Configuration = value
		default:
			if key[:1] == "-" {
				return errors.New(fmt.Sprintf("invalid parameter %v", key))
			}

			if backupName != "" {
				return errors.New("more than one backup job specified")
			}

			backupName = key
		}
	}

	if backupName == "" {
		return errors.New("no backup job specified")
	}

	cmd.Parameters["name"] = backupName

	return nil
}
