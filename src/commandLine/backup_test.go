package commandLine

import (
	"strings"
	"testing"
)

func TestParseCommandLineBackupWithoutFlags(t *testing.T) {
	args := []string{"exe", "backup", "job"}

	cmd, err := ParseCommandLine(args)

	if err != nil {
		t.Errorf("err should be nil, but is not: %v", err)
	}

	if cmd == nil {
		t.Errorf("cmd should be set, but is nil")
	}

	if cmd.Command != Backup {
		t.Errorf("cmd.Command should be 'Backup', but is not: %v", cmd.Command)
	}

	if name, ok := cmd.Parameters["name"]; !ok {
		t.Error("cmd.Parameters['name'] should be set, but is not")
	} else if name != "job" {
		t.Errorf("cmd.Parameters['name'] should be 'job', but is not: %v", name)
	}

	if cmd.Configuration != "/etc/sbackup.conf" {
		t.Errorf("cmd.Configuration should be default path, but is: %v", cmd.Configuration)
	}
}

func TestParseCommandLineBackupWithoutJobName(t *testing.T) {
	args := []string{"exe", "backup"}

	cmd, err := ParseCommandLine(args)

	if cmd != nil {
		t.Errorf("cmd should not be set, but is: %v", cmd)
	}

	if err == nil {
		t.Error("err should not be nil, but is")
	}
}

func TestParseCommandLineBackupWithMultipleJobNames(t *testing.T) {
	args := []string{"exe", "backup", "job1", "job2"}

	cmd, err := ParseCommandLine(args)

	if cmd != nil {
		t.Errorf("cmd should not be set, but is: %v", cmd)
	}

	if err == nil {
		t.Error("err should not be nil, but is")
	}
}

func TestParseCommandLineBackupWithUnknownFlag(t *testing.T) {
	args := []string{"exe", "backup", "job", "--unknown"}

	cmd, err := ParseCommandLine(args)

	if cmd != nil {
		t.Errorf("cmd should not be set, but is: %v", cmd)
	}

	if err == nil {
		t.Error("err should not be nil, but is")
	}

}

func TestParseCommandLineBackupWithConfigFlag(t *testing.T) {
	variants := [][]string{
		{"-c", "something.conf"},
		{"--configuration=something.conf"},
	}

	for _, variant := range variants {
		t.Run(strings.Join(variant, " "), func(t *testing.T) {
			args := []string{"exe", "backup", "job"}
			args = append(args, variant...)

			cmd, err := ParseCommandLine(args)

			if err != nil {
				t.Errorf("err should be nil, but is not: %v", err)
			}

			if cmd == nil {
				t.Errorf("cmd should be set, but is nil")
			}

			if cmd.Configuration != "something.conf" {
				t.Errorf("cmd.Configuration should be 'something.conf', but is: %v", cmd.Configuration)
			}
		})
	}
}
