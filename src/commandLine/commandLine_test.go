package commandLine

import (
	"testing"
)

func TestParseCommandLineWithoutParameters(t *testing.T) {
	args := []string{"exe"}

	cmd, err := ParseCommandLine(args)

	if err != nil {
		t.Errorf("err should be nil, but is not: %v", err)
	}

	if cmd != nil {
		t.Errorf("cmd should be nil, but is set: %v", cmd)
	}
}

func TestParseCommandLineWithHelpParameters(t *testing.T) {
	variants := []string{
		"help",
		"--help",
		"-h",
	}

	for _, variant := range variants {
		t.Run(variant, func(t *testing.T) {
			args := []string{"exe", variant}

			cmd, err := ParseCommandLine(args)

			if err != nil {
				t.Errorf("err should be nil, but is not: %v", err)
			}

			if cmd == nil {
				t.Errorf("cmd should be set, but is nil")
			}

			if cmd.Command != Help {
				t.Errorf("cmd.Command should be Help, but is not: %v", cmd.Command)
			}

			if cmd.ShowHelp != true {
				t.Errorf("cmd.ShowHelp should be true, but is false")
			}
		})
	}
}

func TestParseCommandLineCommandWithHelpFlag(t *testing.T) {
	variants := []struct {
		cs string
		cc Command
	}{
		{cs: "backup", cc: Backup},
		{cs: "clean", cc: Clean},
		{cs: "validate", cc: Validate},
	}
	helpFlags := []string{
		"-h",
		"--help",
	}

	for _, variant := range variants {
		for _, helpFlag := range helpFlags {
			t.Run(variant.cs+" "+helpFlag, func(t *testing.T) {
				args := []string{"exe", variant.cs, helpFlag}

				cmd, err := ParseCommandLine(args)

				if err != nil {
					t.Errorf("err should be nil, but is not: %v", err)
				}

				if cmd == nil {
					t.Errorf("cmd should be set, but is nil")
				}

				if cmd.Command != variant.cc {
					t.Errorf("cmd.Command should be Help, but is not: %v", cmd.Command)
				}

				if cmd.ShowHelp != true {
					t.Errorf("cmd.ShowHelp should be true, but is false")
				}
			})
		}
	}
}

func TestParseCommandLineUnknownCommand(t *testing.T) {
	args := []string{"exe", "unknown"}

	cmd, err := ParseCommandLine(args)

	if cmd != nil {
		t.Errorf("cmd should be nil, but is set: %v", cmd)
	}

	if err == nil {
		t.Error("err should not be nil, but is")
	}
}
