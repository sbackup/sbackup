package main

import (
	"log"
	"os"
	"sbackup/src/business"
	"sbackup/src/commandLine"
	"sbackup/src/configFile"
	"sbackup/src/help"
)

func main() {
	cmd, err := commandLine.ParseCommandLine(os.Args)
	if err != nil {
		log.Fatal(err)
	}

	if cmd == nil || cmd.ShowHelp {
		help.PrintHelp(cmd.Command)
		return
	}

	if cmd.Command == commandLine.Validate {
		log.Println("Validate configuration")
	}

	config, err := configFile.ParseConfigFile(cmd.Configuration)
	if err != nil {
		log.Fatal(err)
	}

	if cmd.Command == commandLine.Validate {
		log.Println("Configuration validated successfully")
		return
	}

	if cmd.Command == commandLine.Backup {
		if _, ok := config.Backups[cmd.Parameters["name"]]; !ok {
			log.Fatalf("'%v' is not configured as backup\n", cmd.Parameters["name"])
		}

		err = business.Backup(cmd.Parameters["name"], config)
		if err != nil {
			log.Fatal(err)
		}

		err = business.Housekeeping(cmd.Parameters["name"], config)
		if err != nil {
			log.Fatal(err)
		}
	}

	if cmd.Command == commandLine.Clean {
		for name := range config.Backups {
			log.Printf("Clean backup configuration '%v'\n", name)
			err = business.Housekeeping(name, config)
			if err != nil {
				log.Fatal(err)
			}
		}
	}
}
