package business

import (
	"fmt"
	"log"
	"sbackup/src/configFile"
	"sort"
	"strings"
)

func Housekeeping(name string, config *configFile.Configuration) error {
	configuration := config.Backups[name]

	if configuration.KeepLast > 0 {
		log.Println("Check old backups")

		out, err := execCommand(false, "rclone", "lsf", fmt.Sprintf("%v/%v", config.BaseDestination, name)).Output()
		if err != nil {
			return err
		}

		backups := deleteEmpty(strings.Split(string(out), "\n"))
		sort.Strings(backups)

		if len(backups) > configuration.KeepLast {
			log.Printf("Found %d old backups to delete", len(backups)-configuration.KeepLast)

			for i := 0; i < len(backups)-configuration.KeepLast; i++ {

				purgePath := fmt.Sprintf("%v/%v/%v", config.BaseDestination, name, backups[i])
				log.Printf("Delete %s", purgePath)

				if isFile(purgePath) {
					err = execCommand(true, "rclone", "deletefile", purgePath).Run()
					if err != nil {
						return err
					}
				} else {
					err = execCommand(true, "rclone", "purge", purgePath).Run()
					if err != nil {
						return err
					}
				}
			}
			log.Println("Old backups deleted successfully")
		} else {
			log.Println("No old backups to delete")
		}
	}

	return nil
}

func isFile(path string) bool {
	return strings.HasSuffix(path, ".zip") || strings.HasSuffix(path, ".tar.gz")
}

func deleteEmpty(s []string) []string {
	var r []string
	for _, str := range s {
		if str != "" {
			r = append(r, str)
		}
	}
	return r
}
