package business

import (
	"os"
	"os/exec"
)

type execCommandFunc func(pipeStd bool, name string, arg ...string) execCmd

type execCmd interface {
	Output() ([]byte, error)
	Run() error
}

var execCommand execCommandFunc = defaultExecCommand

func defaultExecCommand(pipeStd bool, name string, arg ...string) execCmd {
	cmd := exec.Command(name, arg...)
	if pipeStd {
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
	}
	return cmd
}
