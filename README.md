# sbackup

sbackup is a simple backup tool that uses [rclone](https://rclone.org/) for copying files to several destinations. You can define backup configurations with different source paths and different destination paths.

If you want to use a remote location as destination, you have to define this location in the rclone configuration first.

sbackup does not execute backup jobs automatically. You have to execute the backup command either manually or with a tool like cron.

## Configuration

sbackup needs a configuration file where you can specify different backup jobs. The configuration file is formatted like an ini file.

The default location for the configuration file is `/etc/sbackup.conf`. If you use a different location or a different name for the configuration file, you have to specify the location for each command.

Example file:
```
baseDestination=objectstorage:backups

[mybackup]
source=/path/to/somewhere
keepLast=5
compression=zip
```

The first section has no section title and contains this options.

| Option          | Type   | Required | Description                                                                   |
|-----------------|--------|----------|-------------------------------------------------------------------------------|
| baseDestination | string | yes      | rclone path to any rclone compatible destination. Can include subdirectories. |

For each backup job, you want to define, create an own section. The section name becomes the name of the backup job. A backup job section has this options.

| Option      | Type   | Required | Description                                                                                                                                |
|-------------|--------|----------|--------------------------------------------------------------------------------------------------------------------------------------------|
| source      | string | yes      | Path to a local directory.                                                                                                                 |
| keepLast    | number | no       | Number of backups to keep. If more backups present, sbackup deletes the oldest backups. If set to 0, all backups are kept.                 |
| compression | string | no       | Compresses the source directory and uploads the resulting binary.<br> - `zip`: Creates a .zip file.<br> - `targz`: Creates a .tar.gz file. |

## Usage

### backup
Run a specific backup

```shell
sbackup backup mybackup [-c /path/to/configfile]
```

### clean
Remove old backups after you decreased the keepLast property in the configuration file.

```shell
sbackup clean [-c /path/to/configfile]
```

### validate
Validate the configuration file without running a backup

```shell
sbackup validate [-c /path/to/configfile]
```
